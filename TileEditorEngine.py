from configs import GameConfig
from libs import libtcodpy as tcod
from tileditor.TileEditor import TileEditor


class TileEditorEngine:
    def __init__(self):
        self._tileEditor = TileEditor(self.quitTileEditor)
        self._running = False

    def start(self):
        self._initialise()

    def _initialise(self):
        tcod.console_init_root(GameConfig.SCREEN_WIDTH, GameConfig.SCREEN_HEIGHT, GameConfig.GAME_TITLE)
        tcod.sys_set_fps(GameConfig.MAX_FPS)
        self._loop()

    def _loop(self):
        assert isinstance(self._tileEditor, TileEditor)
        key = tcod.Key()
        mouse = tcod.Mouse()
        self._running = True
        while not tcod.console_is_window_closed() and self._running:
            tcod.console_clear(0)
            self._tileEditor.draw(0)
            tcod.console_flush()
            tcod.sys_check_for_event(tcod.EVENT_KEY_PRESS, key, mouse)
            self._tileEditor.handleKey(key)
            self._tileEditor.update(tcod.sys_get_last_frame_length())

    def quitTileEditor(self):
        self._running = False

if __name__ == "__main__":
    GameConfig.GAME_TITLE = b"TCOD Tile Editor"
    GameConfig.GAME_VERSION = "0.1"
    tee = TileEditorEngine()
    tee.start()

