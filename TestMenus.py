from menus.Menu import Menu
from menus.MenuItem import LabelMenuItem, ValueSliderMenuItem, StringInputMenuItem, BlankMenuItem, IntegerInputMenuItem
from libs import libtcodpy as tcod

running = True
meny = Menu(10, 10, maxShownItems=200)
hideTimer = 0
hideTimeout = 3.0


def testMain():
    global hideTimer
    global hideTimeout
    meny.setActive(True)
    meny.addItem(LabelMenuItem("Method 1", callback=lambda: print('method1'), fgcolor=tcod.red))
    meny.addItem(LabelMenuItem("Method 2", callback=method2))
    meny.addItem(LabelMenuItem("Disabled item", None, enabled=False))
    meny.addItem(ValueSliderMenuItem("Test value menu", initialValue=0, step=1, maximum=20, enabled=True))
    meny.addItem(StringInputMenuItem("Test string input menu", 12))
    meny.addItem(LabelMenuItem("Hide me for {} seconds".format(hideTimeout), callback=hideTemp))
    meny.addItem(IntegerInputMenuItem("Test int input", True, minimumValue=-10, maximumValue=300))
    meny.addItem(BlankMenuItem())
    meny.addItem(LabelMenuItem("Exit", callback=exitTest, fgcolor=tcod.dark_red))

    tcod.console_init_root(120, 80, b"Menu test")
    tcod.sys_set_fps(60)

    key = tcod.Key()
    mouse = tcod.Mouse()
    while not tcod.console_is_window_closed() and running:
        tcod.console_clear(0)
        meny.drawMenu(0)
        tcod.sys_check_for_event(tcod.EVENT_KEY_PRESS, key, mouse)
        if meny.isHidden():
            tcod.console_print(0, 1, 1, "{}s left...".format(hideTimeout - hideTimer))
            hideTimer += tcod.sys_get_last_frame_length()
            if hideTimer >= hideTimeout:
                hideTimer = 0.0
                meny.setHidden(False)
        if meny.isActive():
            meny.handleKey(key)
        tcod.console_flush()


def method2():
    print('method2')


def hideTemp():
    meny.setHidden(True)


def exitTest():
    global running
    running = False


if __name__ == "__main__":
    testMain()
