from libs import libtcodpy as tcod

# TODO: Rewrite this into a class, with a dictionary of keys, rebind methods, etc...

# General buttons
CONFIRM_ACTION = tcod.KEY_ENTER
# Specific to ValueMenuItem
VALUE_MENUITEM_INCREASE = tcod.KEY_RIGHT
VALUE_MENUITEM_DECREASE = tcod.KEY_LEFT
# Specific to StringInputMenuItem
ENTER_EDIT_MODE = tcod.KEY_ENTER
END_EDIT_MODE = tcod.KEY_ENTER  # SUPARINTERLINKAGE!
