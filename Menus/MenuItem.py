import abc
import sys

import KeysConfig
from configs import LibConfig
from libs import libtcodpy as tcod
from menus.CustomTCODInput import CustomTCODStringInput, CustomTCODIntegerInput


class MenuItem(metaclass=abc.ABCMeta):
    def __init__(self,
                 enabled,
                 fgcolor,
                 bgcolor,
                 disabledfgColor,
                 disabledBgColor,
                 selectedFgColor,
                 selectedBgColor):
        self._enabled = enabled
        self._fgcolor = fgcolor
        self._bgcolor = bgcolor
        self._disabledFgColor = disabledfgColor
        self._disabledBgColor = disabledBgColor
        self._selectedFgColor = selectedFgColor
        self._selectedBgColor = selectedBgColor
        self._position = -1
        self._selected = False

    def isEnabled(self):
        return self._enabled

    def setEnabled(self, enabled: bool):
        self._enabled = enabled

    def getSelected(self):
        return self._selected

    def setSelected(self, selected: bool):
        self._selected = selected

    def getPosition(self):
        return self._position

    def setPosition(self, newPosition: int):
        self._position = newPosition

    @abc.abstractclassmethod
    def drawItem(self, console: int, onScreenPosition: dict):
        """
        Draws the item itself on the screen.
        :param console: The console the MenuItem must be drawn onto.
        :param onScreenPosition: The position dictionary ('x', 'y') where to draw the MenuItem
        :return: Nothing
        """
        pass

    @abc.abstractclassmethod
    def handleKey(self, pressedKey: tcod.Key):
        """
        Key handling for this MenuItem type. See KeysConfig.py for key configuration
        :param pressedKey: The key that was pressed
        :return: True, if the key was consumed, False otherwise.
        """
        pass


class BlankMenuItem(MenuItem):
    def __init__(self,
                 fgcolor=tcod.white,
                 bgcolor=tcod.black,
                 disabledfgColor=tcod.dark_gray,
                 disabledBgColor=tcod.black,
                 selectedFgColor=tcod.black,
                 selectedBgColor=tcod.white):
        super().__init__(False, fgcolor, bgcolor, disabledfgColor, disabledBgColor, selectedFgColor, selectedBgColor)

    def drawItem(self, console: int, onScreenPosition: dict):
        return

    def handleKey(self, pressedKey: tcod.Key):
        return False


class LabelMenuItem(MenuItem):
    def __init__(self, label: str,
                 callback,
                 enabled=True,
                 fgcolor=tcod.white,
                 bgcolor=tcod.black,
                 disabledfgColor=tcod.dark_gray,
                 disabledBgColor=tcod.black,
                 selectedFgColor=tcod.black,
                 selectedBgColor=tcod.white):
        super().__init__(enabled, fgcolor, bgcolor, disabledfgColor, disabledBgColor, selectedFgColor, selectedBgColor)
        self.callback = callback
        self.label = label

    def executeCallback(self):
        if self.callback is not None:
            self.callback()

    def drawItem(self, console: int, onScreenPosition: dict):
        if console is None:
            return
        definitiveLabel = self.label
        if self._selected:
            definitiveLabel = "> {} <".format(definitiveLabel)
        length = len(definitiveLabel)
        tcod.console_print(console, onScreenPosition['x'], onScreenPosition['y'], definitiveLabel)
        if self._selected:
            for x in range(onScreenPosition['x'], onScreenPosition['x'] + length):
                tcod.console_set_char_foreground(console, x, onScreenPosition['y'], self._selectedFgColor)
                tcod.console_set_char_background(console, x, onScreenPosition['y'], self._selectedBgColor)
        if not self._enabled:
            for x in range(onScreenPosition['x'], onScreenPosition['x'] + length + 1):
                tcod.console_set_char_foreground(console, x, onScreenPosition['y'], self._disabledFgColor)
                tcod.console_set_char_background(console, x, onScreenPosition['y'], self._disabledBgColor)

    def handleKey(self, pressedKey: tcod.Key):
        if pressedKey.vk != 65:  # if it's a special key like enter, etc...
            if pressedKey.vk == KeysConfig.CONFIRM_ACTION:
                self.executeCallback()
                return True
        return False


class ValueSliderMenuItem(MenuItem):
    def __init__(self,
                 label: str,
                 initialValue=0,
                 step=1,
                 minimum=0,
                 maximum=sys.maxsize,
                 enabled=True,
                 fgcolor=tcod.white,
                 bgcolor=tcod.black,
                 disabledfgColor=tcod.dark_gray,
                 disabledBgColor=tcod.black,
                 selectedFgColor=tcod.black,
                 selectedBgColor=tcod.white):
        self._label = label
        self._currentValue = initialValue
        self._step = step
        if maximum < minimum:
            tmp = maximum
            maximum = minimum
            minimum = tmp
        self._minimum = minimum
        self._maximum = maximum
        super().__init__(enabled, fgcolor, bgcolor, disabledfgColor, disabledBgColor, selectedFgColor, selectedBgColor)

    def drawItem(self, console: int, onScreenPosition: dict):
        if console is None:
            return

        definitiveLabel = self._label
        if not self._selected:
            definitiveLabel = '{}: {}'.format(definitiveLabel, self._currentValue)
        if self._selected:
            if self._currentValue == self._minimum:
                definitiveLabel = '{}: {} >'.format(definitiveLabel, self._currentValue)
            elif self._currentValue == self._maximum:
                definitiveLabel = '{}: < {}'.format(definitiveLabel, self._currentValue)
            else:
                definitiveLabel = '{}: < {} >'.format(definitiveLabel, self._currentValue)

        length = len(definitiveLabel)
        tcod.console_print(console, onScreenPosition['x'], onScreenPosition['y'], definitiveLabel)
        if self._selected:
            for x in range(onScreenPosition['x'], onScreenPosition['x'] + length):
                tcod.console_set_char_foreground(console, x, onScreenPosition['y'], self._selectedFgColor)
                tcod.console_set_char_background(console, x, onScreenPosition['y'], self._selectedBgColor)
        if not self._enabled:
            for x in range(onScreenPosition['x'], onScreenPosition['x'] + length + 1):
                tcod.console_set_char_foreground(console, x, onScreenPosition['y'], self._disabledFgColor)
                tcod.console_set_char_background(console, x, onScreenPosition['y'], self._disabledBgColor)

    def handleKey(self, pressedKey: tcod.Key):
        if pressedKey.vk != tcod.KEY_CHAR:
            if pressedKey.vk == KeysConfig.VALUE_MENUITEM_INCREASE:
                self._currentValue += self._step
                if self._currentValue > self._maximum:
                    self._currentValue = self._maximum
                return True
            if pressedKey.vk == KeysConfig.VALUE_MENUITEM_DECREASE:
                self._currentValue -= self._step
                if self._currentValue < self._minimum:
                    self._currentValue = self._minimum
                return True
        return False


class StringInputMenuItem(MenuItem):  # For single line string input only, multiline will come later.
    def __init__(self,
                 label,
                 maxInputSize: int,
                 enabled=True,
                 activatedFgColor=tcod.red,
                 activatedBgColor=tcod.black,
                 fgcolor=tcod.white,
                 bgcolor=tcod.black,
                 disabledfgColor=tcod.dark_gray,
                 disabledBgColor=tcod.black,
                 selectedFgColor=tcod.black,
                 selectedBgColor=tcod.white,
                 showCursor=True,
                 keyTypedCallback=None,
                 clearOnEnter=False):
        self._label = label
        self._editing = False
        self._activatedFgColor = activatedFgColor
        self._activatedBgColor = activatedBgColor
        self._inputManager = CustomTCODStringInput(maxInputSize)
        self._showCursor = showCursor
        self._keyTypedCallback = keyTypedCallback
        self._clearOnEnter = clearOnEnter
        super().__init__(enabled, fgcolor, bgcolor, disabledfgColor, disabledBgColor,
                         selectedFgColor, selectedBgColor)

    def drawItem(self, console: int, onScreenPosition: dict):
        if console is None:
            return

        definitiveLabel = '{}: {}'.format(self._label, self._inputManager.getBuffer())
        if self._selected:
            pass
        length = len(definitiveLabel)
        if self._showCursor and self._inputManager.getSize() < self._inputManager.getMaxInputSize() and self._editing:
            definitiveLabel = '{}_'.format(definitiveLabel)
            length += 1

        tcod.console_print(console, onScreenPosition['x'], onScreenPosition['y'], definitiveLabel)
        if self._selected:
            for x in range(onScreenPosition['x'], onScreenPosition['x'] + length):
                tcod.console_set_char_foreground(console, x, onScreenPosition['y'], self._selectedFgColor)
                tcod.console_set_char_background(console, x, onScreenPosition['y'], self._selectedBgColor)
        if not self._enabled:
            for x in range(onScreenPosition['x'], onScreenPosition['x'] + length + 1):
                tcod.console_set_char_foreground(console, x, onScreenPosition['y'], self._disabledFgColor)
                tcod.console_set_char_background(console, x, onScreenPosition['y'], self._disabledBgColor)
        if self._editing:
            for x in range(onScreenPosition['x'], onScreenPosition['x'] + length + 1):
                tcod.console_set_char_foreground(console, x, onScreenPosition['y'], self._activatedFgColor)
                tcod.console_set_char_background(console, x, onScreenPosition['y'], self._activatedBgColor)

    def handleKey(self, pressedKey: tcod.Key):
        # NOT EDITING
        if not self._editing:
            if pressedKey.vk == KeysConfig.ENTER_EDIT_MODE:
                self._editing = True  # Activate the edit mode
                if self._clearOnEnter:
                    self._inputManager.clearBuffer()
                self.executeKeyTypedCallback()
                return True
        # EDITING
        else:
            if pressedKey.vk == KeysConfig.END_EDIT_MODE:
                self._editing = False
                self.executeKeyTypedCallback()
                return True
            if self._inputManager.dispatchKey(pressedKey):
                self.executeKeyTypedCallback()
                return True
            return False
        # end
        return False

    def executeKeyTypedCallback(self, *args, **kwargs):
        if LibConfig.DEBUG_MODE:
            print('Executing \'KeyTyped\' callback, with args: {}, and kwargs: {}'.format(args, kwargs))
        if self._keyTypedCallback is not None:
            self._keyTypedCallback(args, kwargs)

    def isEditing(self):
        return self._editing


class IntegerInputMenuItem(MenuItem):
    def __init__(self,
                 label: str,
                 showMinMaxInLabel=False,
                 maximumValue=sys.maxsize,
                 minimumValue=-sys.maxsize-1,
                 enabled=True,
                 activatedFgColor=tcod.red,
                 activatedBgColor=tcod.black,
                 fgcolor=tcod.white,
                 bgcolor=tcod.black,
                 disabledfgColor=tcod.dark_gray,
                 disabledBgColor=tcod.black,
                 selectedFgColor=tcod.black,
                 selectedBgColor=tcod.white):
        self._label = label
        self._activatedFgColor = activatedFgColor
        self._activatedBgColor = activatedBgColor
        self._editing = False
        if maximumValue < minimumValue:
            tmp = maximumValue
            maximumValue = minimumValue
            minimumValue = tmp
        self._maxValue = maximumValue
        self._minValue = minimumValue
        self._showMinMaxInLabel = showMinMaxInLabel
        self._inputManager = CustomTCODIntegerInput(maxValue=self._maxValue, minValue=self._minValue)

        super().__init__(enabled, fgcolor, bgcolor, disabledfgColor, disabledBgColor, selectedFgColor, selectedBgColor)

    def drawItem(self, console: int, onScreenPosition: dict):
        if console is None:
            return

        definitiveLabel = self._label
        if self._showMinMaxInLabel:
            definitiveLabel = '{} {}'.format(definitiveLabel, '({} to {})'.format(self._minValue, self._maxValue))
        definitiveLabel = '{}: {}'.format(definitiveLabel, self._inputManager.getBuffer())
        length = len(definitiveLabel)
        tcod.console_print(console, onScreenPosition['x'], onScreenPosition['y'], definitiveLabel)
        if self._selected:
            for x in range(onScreenPosition['x'], onScreenPosition['x'] + length):
                tcod.console_set_char_foreground(console, x, onScreenPosition['y'], self._selectedFgColor)
                tcod.console_set_char_background(console, x, onScreenPosition['y'], self._selectedBgColor)
        if not self._enabled:
            for x in range(onScreenPosition['x'], onScreenPosition['x'] + length + 1):
                tcod.console_set_char_foreground(console, x, onScreenPosition['y'], self._disabledFgColor)
                tcod.console_set_char_background(console, x, onScreenPosition['y'], self._disabledBgColor)
        if self._editing:
            for x in range(onScreenPosition['x'], onScreenPosition['x'] + length + 1):
                tcod.console_set_char_foreground(console, x, onScreenPosition['y'], self._activatedFgColor)
                tcod.console_set_char_background(console, x, onScreenPosition['y'], self._activatedBgColor)

    def handleKey(self, pressedKey: tcod.Key):
        # NOT EDITING
        if not self._editing:
            if pressedKey.vk == KeysConfig.ENTER_EDIT_MODE:
                self._editing = True  # Activate the edit mode
                self._inputManager.clearBuffer()
                return True
        # EDITING
        else:
            if pressedKey.vk == KeysConfig.END_EDIT_MODE:
                self._editing = False
                return True
            self._inputManager.dispatchKey(pressedKey)
            return True
        # end
        return False

    def isEditing(self):
        return self._editing
