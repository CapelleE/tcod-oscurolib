from menus.MenuItem import MenuItem, StringInputMenuItem
from libs import libtcodpy as tcod


class Menu:
    def __init__(self, posX=0, posY=0, maxShownItems=10, border=False):
        self._items = []
        self._selected = None
        self._active = False  # Is the menu currently being used
        self._border = border  # Border is unused for now
        self._posX = posX
        self._posY = posY
        self._hidden = False
        self._block = False  # Block on currently selected option, useful for string input, etc...
        self._maxShownItems = maxShownItems - 1
        self._start = 0
        self._end = self._maxShownItems

    def addItem(self, item: MenuItem):
        self._items.append(item)
        item._position = self.getSize() - 1  # Index, not actual size
        if self._selected is None and item.isEnabled():
            self._selected = item
            self._selected.setSelected(True)

    def getSize(self):
        return len(self._items)

    def isHidden(self):
        return self._hidden

    def setHidden(self, hidden: bool):
        self._hidden = hidden

    def getSelectedItem(self):
        return self._selected

    def drawMenu(self, console: int):
        if self._hidden:
            return
        if self._start > 0:
            tcod.console_print(console, self._posX, self._posY - 1, "...")
        if self._end < self.getSize() - 1:
            tcod.console_print(console, self._posX, self._posY + 1 + self._maxShownItems, "...")
        if self._maxShownItems > tcod.console_get_height(console):
            self._maxShownItems = tcod.console_get_height(console) - 1
        if self._maxShownItems > self.getSize():
            self._maxShownItems = self.getSize() - 1
        for i in range(0, self._maxShownItems + 1):
            assert isinstance(self._items[i], MenuItem)
            index = self._items[self._start + i].getPosition()
            posX = self._posX
            posY = self._posY + i
            self._items[index].drawItem(console, {'x': posX, 'y': posY})
        tcod.console_flush()

    def hasAtLeastOneEnabledItem(self):
        for item in self._items:
            if item.isEnabled():
                return True
        return False

    def selectPreviousItem(self):
        if self.getSize() == 0:
            return
        if not self.hasAtLeastOneEnabledItem():
            return
        currentIndex = self._selected.getPosition()
        prevIndex = currentIndex - 1
        if prevIndex < 0:
            prevIndex = self.getSize() - 1
        self._selected.setSelected(False)
        self._selected = self._items[prevIndex]
        self._selected.setSelected(True)
        if self._selected.getPosition() < self._start or self._selected.getPosition() > self._end:
            self.scrollUp()
        if not self._selected.isEnabled():
            self.selectPreviousItem()

    def selectNextItem(self):
        if self.getSize() == 0:
            return
        if not self.hasAtLeastOneEnabledItem():
            return
        currentIndex = self._selected.getPosition()
        nextIndex = currentIndex + 1
        if nextIndex == self.getSize():
            nextIndex = 0
        self._selected.setSelected(False)
        self._selected = self._items[nextIndex]
        self._selected.setSelected(True)

        if self._selected.getPosition() > self._end or self._selected.getPosition() < self._start:
            self.scrollDown()

        if not self._selected.isEnabled():
            self.selectNextItem()

    def scrollUp(self):
        self._start -= 1
        self._end -= 1
        if self._start < 0:
            self._start = self.getSize() - self._maxShownItems - 1
            self._end = self.getSize() - 1

    def scrollDown(self):
        self._start += 1
        self._end += 1
        if self._end >= self.getSize():
            self._start = 0
            self._end = self._maxShownItems

    def isBlocked(self):
        return self._block

    def isActive(self):
        return self._active

    def setActive(self, active: bool):
        self._active = active

    def handleKey(self, pressedKey: tcod.Key):
        if pressedKey.vk == tcod.KEY_NONE:
            return False
        print('{} or {}'.format(pressedKey.vk, chr(pressedKey.c)))

        if self._hidden:
            return False

        if not self._active:
            return False

        # Special case for string input MenuItem
        if isinstance(self._selected, StringInputMenuItem):
            self._block = self._selected.isEditing()
        # End special case

        if self._selected.handleKey(pressedKey):
            return True

        if not self.isBlocked():
            if pressedKey.vk == tcod.KEY_UP:
                self.selectPreviousItem()
                return True
            if pressedKey.vk == tcod.KEY_DOWN:
                self.selectNextItem()
                return True
        return False
