import sys

from libs import libtcodpy as tcod


class CustomTCODStringInput:
    def __init__(self, maxInputSize: int, minInputSize=0, defaultValue='', acceptNumbers=True):
        self._maxInputSize = maxInputSize
        self._minInputSize = minInputSize
        self._buffer = ''
        self._defaultValue = defaultValue
        self._acceptNumbers = acceptNumbers
        self._validSpecialKeys_numbers = [
            tcod.KEY_0,
            tcod.KEY_1,
            tcod.KEY_2,
            tcod.KEY_3,
            tcod.KEY_4,
            tcod.KEY_5,
            tcod.KEY_6,
            tcod.KEY_7,
            tcod.KEY_8,
            tcod.KEY_9,
            tcod.KEY_KP0,
            tcod.KEY_KP1,
            tcod.KEY_KP2,
            tcod.KEY_KP3,
            tcod.KEY_KP4,
            tcod.KEY_KP5,
            tcod.KEY_KP6,
            tcod.KEY_KP7,
            tcod.KEY_KP8,
            tcod.KEY_KP9,
        ]

    def dispatchKey(self, pressed: tcod.Key):
        if pressed.vk is not tcod.KEY_CHAR:
            if pressed.vk == tcod.KEY_BACKSPACE:
                if len(self._buffer) > 0:
                    self._buffer = self._buffer[:-1]
                return True
            if pressed.vk == tcod.KEY_SPACE and self.getSize() < self._maxInputSize:
                self._buffer = '{} '.format(self._buffer)
                return True
            if pressed.vk in self._validSpecialKeys_numbers and self._acceptNumbers and self.getSize() < self._maxInputSize:
                self._buffer = '{}{}'.format(self._buffer, chr(pressed.c))
                return True
        else:
            if self.getSize() == self._maxInputSize:
                return False
            letter = chr(pressed.c)
            self._buffer = '{}{}'.format(self._buffer, letter)
            return True
        return False

    def getBuffer(self):
        return self._buffer

    def getSize(self):
        return len(self._buffer)

    def clearBuffer(self):
        self._buffer = self._defaultValue

    def getMaxInputSize(self):
        return self._maxInputSize

    def getMinInputSize(self):
        return self._minInputSize


class CustomTCODIntegerInput:
    def __init__(self, maxValue=sys.maxsize, minValue=0):
        self._maxValue = maxValue
        self._minValue = minValue
        self._valid = [
            tcod.KEY_0,
            tcod.KEY_1,
            tcod.KEY_2,
            tcod.KEY_3,
            tcod.KEY_4,
            tcod.KEY_5,
            tcod.KEY_6,
            tcod.KEY_7,
            tcod.KEY_8,
            tcod.KEY_9,
            tcod.KEY_KP0,
            tcod.KEY_KP1,
            tcod.KEY_KP2,
            tcod.KEY_KP3,
            tcod.KEY_KP4,
            tcod.KEY_KP5,
            tcod.KEY_KP6,
            tcod.KEY_KP7,
            tcod.KEY_KP8,
            tcod.KEY_KP9,
            tcod.KEY_KPSUB,
            '-'
        ]
        self._buffer = '0'

    def dispatchKey(self, pressed: tcod.Key):
        if pressed.vk is not tcod.KEY_CHAR:
            if pressed.vk == tcod.KEY_BACKSPACE:
                if self.getSize() > 0:
                    self._buffer = self._buffer[:-1]
                    self.__checkBoundaries()
                    return True
                return True
            if pressed.vk in self._valid:
                self._buffer = '{}{}'.format(self._buffer, chr(pressed.c))
                self.__checkBoundaries()
                return True
        else:  # is KEY_CHAR
            if pressed.c in self._valid:
                if chr(pressed.c) == '-':
                    if self.getSize() == 0 or self.getBufferAsInteger() == 0:
                        self._buffer = '-'
            return False
        return False

    def __checkBoundaries(self):
        if self._buffer == '':
            self.clearBuffer()
        if len(self._buffer) > 1 and self._buffer.startswith('0'):
            self._buffer = self._buffer[1:]
        try:
            if int(self._buffer) > self._maxValue:
                self._buffer = str(self._maxValue)
            if int(self._buffer) < self._minValue:
                self._buffer = str(self._minValue)
        except ValueError:
            pass

    def getBuffer(self):
        return self._buffer

    def getBufferAsInteger(self):
        return int(self._buffer)

    def getSize(self):
        return len(self._buffer)

    def clearBuffer(self):
        self._buffer = '0' if 0 > self._minValue else str(self._minValue)
