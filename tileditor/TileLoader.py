import os

import simplejson

from configs import LibConfig
from libs import libtcodpy as tcod
from tileditor.Tile import Tile


class TileLoader:
    def __init__(self):
        self.__filepathFromBase = "data\\tiles.json"
        self.__dataFilePath = "{}\\{}".format(LibConfig.BASE_DIR, self.__filepathFromBase)
        self.__tileDictionary = None
        self._initialised = False

    def initialise(self):
        self.__tileDictionary = self._buildTileDictionary()
        self._initialised = True

    def _testFilePath(self):
        return os.path.isfile(self.__dataFilePath)

    def _getDataFilePath(self):
        return self.__dataFilePath

    def _loadAllTiles(self):
        try:
            with open(self.__dataFilePath, 'r') as fp:
                retVal = simplejson.load(fp)
                return retVal
        except FileNotFoundError:
            print('Tiles data file json not found!')
            return {}

    def _buildTileDictionary(self):
        dictionary = {}
        for k, v in self._loadAllTiles().items():
            tile = Tile(
                k,
                v['graphic'],
                tcod.Color(
                    v['fgcolor']['r'],
                    v['fgcolor']['g'],
                    v['fgcolor']['b']
                ),
                tcod.Color(
                    v['bgcolor']['r'],
                    v['bgcolor']['g'],
                    v['bgcolor']['b']
                )
            )
            if dictionary.get(k, None) is not None:
                print('Warning: collision for key {} in tiles dictionary! Latest item skipped.'.format(k))
            else:
                dictionary[k] = tile

        self._initialised = True
        return dictionary

    def getTileDictionary(self):
        return self.__tileDictionary

    def isInitialised(self):
        return self._initialised
