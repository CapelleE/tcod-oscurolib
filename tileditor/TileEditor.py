from libs import libtcodpy as tcod
from tileditor.TileLoader import TileLoader


class TileEditor:
    def __init__(self, tileEditorExitCallback):
        self._tileLoader = TileLoader()
        self._tileEditorExitCallback = tileEditorExitCallback

    def initialise(self):
        if not self._tileLoader.isInitialised():
            self._tileLoader.initialise()

    def _getAllTiles(self):
        return self._tileLoader.getTileDictionary()

    def _executeTileEditorExitCallback(self):
        if self._tileEditorExitCallback is not None:
            self._tileEditorExitCallback()

    def update(self, deltaTime: float):
        pass

    def draw(self, targetTCODConsole: int):
        pass

    def handleKey(self, pressedKey: tcod.Key):
        return False
