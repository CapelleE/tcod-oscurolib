from libs import libtcodpy as tcod


class Tile:
    def __init__(self, name: str, graphic: str, fgcolor: tcod.Color, bgcolor: tcod.Color):
        self.name = name
        self.graphic = graphic
        self.fgcolor = fgcolor
        self.bgcolor = bgcolor

    def draw(self):
        pass

    def __str__(self):
        return "{}: {} - fgcolor: {} - bgcolor: {}".format(self.name, self.graphic, self.fgcolor, self.bgcolor)

    def __repr__(self):
        return "{}: {} - fgcolor: {} - bgcolor: {}".format(self.name, self.graphic, self.fgcolor, self.bgcolor)
